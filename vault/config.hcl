listener "tcp" {
  address     = "127.0.0.1:8200"
  tls_disable = 1
}

storage "file" {
  path = "/tmp/vault"
}

telemetry {
  statsite_address = "127.0.0.1:8126"
  disable_hostname = true
  enable_hostname_label = true
}
